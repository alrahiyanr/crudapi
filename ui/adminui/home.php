<!doctype html>
<html lang="en">
  <head>
  	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Request</title>
    <?php include('cssnjs.php');?>
  </head>
  <body>
  <div class="alert alert-success"><span id="success_message"></span></div>
  <div class="alert alert-error"><span id="error_message"></span></div>
  <div class="container">
      <div class ="row justify-content-center">
        <table class="table">
        <thead>
          <tr>
              <th>ID</th>
              <th>title</th>
              <th>category</th>
              <th>initiator</th>
              <th>initiatoremail</th>
              <th colspan="2">Action</th>
              
          </tr>                     
        </thead>
        <tbody id="table_body"></tbody>
        </table>
        
        </div>
    </div>
    <div>
		<div class="modal fade" id="myModal" role="dialog">
		 <div class="modal-dialog " role = "document">
		 <div class="modal-content">
		 <div class="modal-header">
			 
			 <h4 class="modal-title w-100 font-weight-bold">DELETE</h4>
			 </div>
			 <div class="modal-body">
			    <p>Are you sure want to delete this record</p>
			</div>
			 <div class="modal-footer">
                <button type="button" class="btn btn-info" id="yes" >yes</button>
			     <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
			 </div>
			</div>
			 </div>
			</div>
			
            <div class="modal fade" id="update" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                 <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header text-center">
                            <h4 class="modal-title w-100 font-weight-bold">Edit</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body mx-3">
                            <div class="md-form mb-2">
                                <i class="fa fa-user prefix grey-text"></i>
                                <!-- <input type="text" id="id" class="form-control validate"> -->
                                <select name="" id="id"></select>
                                <label data-error="wrong" data-success="right" for="id">ID</label>
                            </div>

                            <div class="md-form mb-2">
                                <i class="fa fa-envelope prefix grey-text"></i>
                                <input type="email" id="title" class="form-control validate">
                                <label data-error="wrong" data-success="right" for="title">Title</label>
                                </div>

                            <div class="md-form mb-2">
                                <i class="fa fa-tag prefix grey-text"></i>
                                <input type="text" id="category" class="form-control validate">
                                <label data-error="wrong" data-success="right" for="category">Category</label>
                            </div>

                            <div class="md-form mb-2">
                                <i class="fa fa-tag prefix grey-text"></i>
                                <input type="text" id="initiator" class="form-control validate">
                                <label data-error="wrong" data-success="right" for="initiator">Initiator</label>
                            </div>

                            <div class="md-form mb-2">
                                <i class="fa fa-tag prefix grey-text"></i>
                                <input type="text" id="initiatoremail" class="form-control validate">
                                <label data-error="wrong" data-success="right" for="initiatoremail">Initiatoremail</label>
                            </div>
                            

                        </div>
                        <div class="modal-footer d-flex justify-content-center">
                            <button class="btn btn-success" id="yes1">update <i class="fa fa-paper-plane-o ml-1"></i></button>
                        </div>
                    </div>
                </div>
            </div>

   
  </body>
  </html>
  