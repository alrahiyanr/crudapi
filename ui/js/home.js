var home={
		init:function(){
			this.getList();
		},
		getList:function(){
			var _this =  this;
			$.ajax({
				url: "../../api/admin/v1/request.php",
				type: 'GET',
				success: function(result)
				{
					console.log(result);
					_this.renderData(result);
				},
				error:function(result)
				{
					console.log(result);
					$("#error_message").html(result.responseText);
				}
			});	
		},
		renderData:function(data)
		{ 
			var tabledata = "";
			var opt =[];
			for(i in data)
			{
				var thistabledata = '<tr>';
				
				for(j in data[i])
				{
					thistabledata+= "<td>"+data[i][j]+"</td>";
				}
				thistabledata+= '<td><button onclick= "home.b()" type="button" ><img src="../assets/edit.png" width="20" height="20" /></button>	';
				thistabledata+= ' <button onclick= "home.a('+data[i].id+')" type="button"  ><img src="../assets/edit1.png" width="20" height="20" /> </button></td>';	
				thistabledata += "</tr>";
				opt.push("<option>"+data[i].id+"</option>")
				
				tabledata+=thistabledata;
			}
			console.log(opt);
			
			$("#table_body").html(tabledata);

			$("#id").html(opt.toString());
		},
		b:function(){
			$('#update').modal('show');
			$('#yes1').click(function() {
				home.update();
				$('#update').modal('hide');
			})
		},
		
		update:function(){
			var id = document.getElementById('id').value;
			var title = document.getElementById('title').value;
			var category = document.getElementById('category').value;
			var initiator = document.getElementById('initiator').value;
			var initiatoremail = document.getElementById('initiatoremail').value;
			
			var data = {};
			data['id'] = id;
			data['title'] = title;
			data['category'] = category;
			data['initiator'] = initiator;
			data['initiatoremail'] = initiatoremail;
			
			console.log(data)
			$.ajax({
				url: "../../api/admin/v1/request.php/"+id,
				type: 'PUT',
				data: JSON.stringify(data),
				success: function(result)
				{
					console.log(result);
					location.reload();
					
				},
				error:function(result)
				{
					console.log(result);
					$("#error_message").html(result.responseText);
				}
			});	
		},
		a:function(id){
			$('#myModal').modal('show');
			$('#yes').click(function() {
				home.deleteData(id);
				$('#myModal').modal('hide');
			})
		},
		
		deleteData:function(id){
			
			console.log(id)
			
			$.ajax({
				url: "../../api/admin/v1/request.php/"+id,
				type: 'DELETE',
				success: function(result)
				{
					console.log(result);
					location.reload();
					
				},
				error:function(result)
				{
					console.log(result);
					$("#error_message").html(result.responseText);
				}
			});
		}
};
//git
$(document).ready(function(){home.init();});