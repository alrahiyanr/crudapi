<?php
$GLOBALS['errorcodes']=array(
    1000=>"Redis connection error",
    1001=>"Invalid type",
    1002=>"Subscriber creation failed",
    1003=>"Email exists",
    1004=>"Incorrect planlist",
    1005=>"Invalid Object Type",
    1006=>"DB Updation Failed",
    1007=>"Invalid Access Right",
    1008=>"Invalid Activation Code",
    1009=>"DB Error",
    1010=>"Invalid Object Id",
    1056=>"Duplicate entry",
    1057=>"Nothing to update",
    1058=>"Api user device changed",
    2001=>"No Data To Display",
);
?>