<?php
/*********************************************************
 Author: 		Alrahiyan
 Copyright: 	Mobiotics IT Solution Private Limited
 Version: 		1.0
 Date:			
 FileName: 		
 Description:	
 **********************************************************/
error_reporting(E_ALL);

require_once("errorcodes.php");

//Logging Related
define('LLOG_SEPARATOR',',');

//HTTP Errors
define("VLIVE_HTTP_METHOD_NOT_ALLOWED","This method is not allowed");
define("VLIVE_HTTP_INVALID_SESSION","No valid session exist");
define("VLIVE_HTTP_FORBIDDEN_REQUEST","Forbidden Request");
define("VLIVE_HTTP_MISSING_PARAMETER","Missing Parameter");
define("VLIVE_HTTP_UNAUTHORIZED","Unauthorized");
define("VLIVE_HTTP_NOTHING_TO_UPDATE","Nothing To Be Updated");

//Pagination Related
define('DB_PAGELIMIT',15);

//Mysql Related
define("MYSQL_SERVER","localhost");
define("MYSQL_USERNAME","root");
define("MYSQL_PASSWORD","Alrahi@786$");
define("MYSQL_DBNAME","request");
?>