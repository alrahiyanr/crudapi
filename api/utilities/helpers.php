<?php
/*********************************************************
 Author: 		Alrahiyan
 Copyright: 	Mobiotics IT Solution Private Limited
 Version: 		1.0
 Date:			
 FileName: 		
 Description:	
 **********************************************************/
error_reporting(E_ALL);

function formatError($number)
{
    $error = 'Unknown Error';
    
    if(!empty($GLOBALS['errorcodes'][$number]))
    {
        $error = $GLOBALS['errorcodes'][$number];
    }
    
    error_log($number."===>".$error);
    
    return array("errorcode"=>$number,'reason'=>$error);
}

function getMySQLConnection()
{
    $mysqlcon = new mysqli(MYSQL_SERVER,MYSQL_USERNAME,MYSQL_PASSWORD,MYSQL_DBNAME);
    
    if($mysqlcon->connect_errno)
    {
        error_log("Failed to connect to MySQL: (" . $mysqlcon->connect_errno . ") " . $mysqlcon->connect_error);
        HTTPFailWithCode(405,$mysqlcon->connect_error);
    }
    
    if(!$mysqlcon->set_charset('utf8'))
    {
        error_log("Error loading character set utf8: " . $mysqlcon->error);
        HTTPFailWithCode(405,$mysqlcon->error);
    }
    
    return $mysqlcon;
}

function mysql_fix_string($string,$mysqlcon)
{
    $string = strip_tags($string);
    
    $string = trim($string);
    
    $string = preg_replace('/[^[:print:]]/', '',$string);
    
    if(get_magic_quotes_gpc())
    {
        $string = stripslashes($string);
    }
    
    $string = $mysqlcon->real_escape_string($string);
    
    return $string;
}

function HTTPFailWithCode($code,$message)
{
    error_log($code.LLOG_SEPARATOR.$message);
    header(reasonForCode($code));
    exit($message);
}

function reasonForCode($code)
{
    switch ($code)
    {
        case 100: $text = 'Continue'; break;
        case 101: $text = 'Switching Protocols'; break;
        case 200: $text = 'OK'; break;
        case 201: $text = 'Created'; break;
        case 202: $text = 'Accepted'; break;
        case 203: $text = 'Non-Authoritative Information'; break;
        case 204: $text = 'No Content'; break;
        case 205: $text = 'Reset Content'; break;
        case 206: $text = 'Partial Content'; break;
        case 300: $text = 'Multiple Choices'; break;
        case 301: $text = 'Moved Permanently'; break;
        case 302: $text = 'Moved Temporarily'; break;
        case 303: $text = 'See Other'; break;
        case 304: $text = 'Not Modified'; break;
        case 305: $text = 'Use Proxy'; break;
        case 400: $text = 'Bad Request'; break;
        case 401: $text = 'Unauthorized'; break;
        case 402: $text = 'Payment Required'; break;
        case 403: $text = 'Forbidden'; break;
        case 404: $text = 'Not Found'; break;
        case 405: $text = 'Method Not Allowed'; break;
        case 406: $text = 'Not Acceptable'; break;
        case 407: $text = 'Proxy Authentication Required'; break;
        case 408: $text = 'Request Time-out'; break;
        case 409: $text = 'Conflict'; break;
        case 410: $text = 'Gone'; break;
        case 411: $text = 'Length Required'; break;
        case 412: $text = 'Precondition Failed'; break;
        case 413: $text = 'Request Entity Too Large'; break;
        case 414: $text = 'Request-URI Too Large'; break;
        case 415: $text = 'Unsupported Media Type'; break;
        case 500: $text = 'Internal Server Error'; break;
        case 501: $text = 'Not Implemented'; break;
        case 502: $text = 'Bad Gateway'; break;
        case 503: $text = 'Service Unavailable'; break;
        case 504: $text = 'Gateway Time-out'; break;
        case 505: $text = 'HTTP Version not supported'; break;
        default: $text = 'Unknown Error';break;
    }
    
    return 'HTTP/1.1'.' '.$code.' '.$text;
}
?>