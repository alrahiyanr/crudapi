<?php
/*********************************************************
 Author: 		Alrahiyan
 Copyright: 	Mobiotics IT Solution Private Limited
 Version: 		1.0
 Date:			
 FileName: 		
 Description:	
 **********************************************************/
error_reporting(E_ALL);

function getListDetails($params)
{
    $mysqlcon = getMySQLConnection();
    
    $query = sprintf("SELECT * from request");
    
    if(!empty($params['title']))
    {
        $query .= sprintf(" WHERE title = '%s'",mysql_fix_string($params['title'],$mysqlcon));
    }
    
    error_log($query);
    
    if(!($result=$mysqlcon->query($query)))
    {
        error_log("getListDetails===>" . $mysqlcon->errno . ") " . $mysqlcon->error);
        $mysqlcon->close();
        return formatError(1009);
    }
    
    if($result->num_rows == 0)
    {
        error_log("getListDetails===>No Rows");
        $mysqlcon->close();
        return formatError(2001);
    }
    
    $datalist = array();
    
    while($row = $result->fetch_assoc())
    {
        $datalist[] = $row;
    }
    
    $mysqlcon->close();
    
    return $datalist;
}



function updateDetails($id,$data)
{
    $updateData = array();
    
    if(!empty($data['title']))
    {
        $updateData['title'] = $data['title'];
    }
    
    if(!empty($data['category']))
    {
        $updateData['category'] = $data['category'];
    }
    if(!empty($data['initiator']))
    {
        $updateData['initiator'] = $data['initiator'];
    }
    if(!empty($data['initiatoremail']))
    {
        $updateData['initiatoremail'] = $data['initiatoremail'];
    }
    
   
    
    if(empty($updateData))
    {
        return formatError(1057);
    }
    
    $lastkey = key( array_slice( $updateData, -1, 1, TRUE ) );
    
    error_log($lastkey);
    
    $mysqlcon = getMySQLConnection();
    
    $query = sprintf("UPDATE request SET ");
    
    foreach($updateData as $keyname=>$keyvalue)
    {
        $query .= sprintf("%s = '%s'",mysql_fix_string($keyname,$mysqlcon),mysql_fix_string($keyvalue,$mysqlcon));
        
        if($keyname != $lastkey)
        {
            $query .= ",";
        }
    }
    
    $query .= sprintf(" WHERE id = '%s'",mysql_fix_string($id,$mysqlcon));
    
    error_log($query);
    
    if(!($stmt = $mysqlcon->prepare($query)))
    {
        error_log("updateDetails: (" . $mysqlcon->errno . ") " . $mysqlcon->error);
        $mysqlcon->close();
        return formatError(1009);
    }
    
   
    if(!$stmt->execute())
    {
        error_log("updateDetails: (" . $stmt->errno . ") " . $stmt->error);
        $mysqlcon->close();
        return formatError(1009);
    }
    
    if($stmt->affected_rows == 0)
    {
        error_log("Update Device Status : No rows affected ");
        $mysqlcon->close();
        return formatError(1009);
    }
    
    $stmt->close();
    $mysqlcon->close();
    
    return array('success'=>$id);
}

function deleteDetails($id)
{
    $mysqlcon = getMySQLConnection();
    
    $query = sprintf("DELETE FROM request ");
    
    $query .= sprintf(" WHERE id = '%s'",mysql_fix_string($id,$mysqlcon));
    
    error_log($query);
    
    if(!($stmt = $mysqlcon->prepare($query)))
    {
        error_log("deleteDetails: (" . $mysqlcon->errno . ") " . $mysqlcon->error);
        $mysqlcon->close();
        return formatError(1009);
    }
    
    
    if(!$stmt->execute())
    {
        error_log("deleteDetails: (" . $stmt->errno . ") " . $stmt->error);
        $mysqlcon->close();
        return formatError(1009);
    }
    
    if($stmt->affected_rows == 0)
    {
        error_log("Deleted data ");
        $mysqlcon->close();
        return formatError(1009);
    }
    
    $stmt->close();
    $mysqlcon->close();
    
    return array('success'=>$id);
}
?>