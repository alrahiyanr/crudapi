<?php
/*********************************************************
 Author: 		Alrahiyan R
 Copyright: 	Mobiotics IT Solution Private Limited
 Version: 		1.0
 Date:			25-Oct-2018
 FileName: 		request.php
 Description:	
 **********************************************************/
header("Content-Type:application/json");
error_reporting(E_ALL);

require_once("../../includes/const.inc.php");
require_once("../../utilities/helpers.php");
require_once("../../utilities/utilities.php");

function rest_get($request)
{
    $parts = parse_url($request);
    $path_parts = pathinfo($parts['path']);
    $id = $path_parts['filename'];
    
    error_log("REQUEST_API_GET_Endpoint===>".$id);
    error_log("REQUEST_API_GET_Data===>".json_encode($_GET));
    
    $result = getListDetails($_GET);
    
    error_log("REQUEST_API_GET_Result===>".json_encode($result));
    
    print json_encode($result);
}

function rest_post($request)
{
    $parts = parse_url($request);
    $path_parts = pathinfo($parts['path']);
    $id = $path_parts['filename'];
    
    error_log("REQUEST_API_POST_Endpoint===>".$id);
    error_log("REQUEST_API_POST_Data===>".json_encode($_POST));
    
    $result = createDetails($_POST);
    
    error_log("REQUEST_API_POST_Result===>".json_encode($result));
    
    print json_encode($result);
}

function rest_put($request)
{
    $parts = parse_url($request);
    $path_parts = pathinfo($parts['path']);
    $id = $path_parts['filename'];
    
    $put_vars = json_decode(file_get_contents('php://input'),true);
    
    error_log("REQUEST_API_PUT_Endpoint===>".$id);
    error_log("REQUEST__API_PUT_Data===>".json_encode($put_vars));
    
    if(empty($put_vars))
    {
        HTTPFailWithCode(400,"Nothing To Update");
    }
    
    $result = updateDetails($id,$put_vars);
    
    error_log("REQUEST__API_PUT_Result===>".json_encode($result));
    
    print json_encode($result);
}

function rest_delete($request)
{
    $parts = parse_url($request);
    $path_parts = pathinfo($parts['path']);
    $id = $path_parts['filename'];
    
    $result = deleteDetails($id);
    
    error_log("REQUEST__API_DELETE_Result===>".json_encode($result));
    
    print json_encode($result);
}

function rest_error($request)
{
    HTTPFailWithCode(405,HTTP_METHOD_NOT_ALLOWED);
}


if(!isset($_SERVER['REQUEST_METHOD']) || !isset($_SERVER['REQUEST_URI']))
{
    HTTPFailWithCode(400,'HTTP Method or request URI is not set');
}

$method = $_SERVER['REQUEST_METHOD'];
$request = $_SERVER['REQUEST_URI'];

switch($method)
{
    case 'GET':
        rest_get($request);
        break;
        
    case 'POST':
        rest_post($request);
        break;
        
    case 'PUT':
        rest_put($request);
        break;
        
    case 'DELETE':
        rest_delete($request);
        break;
        
    default:
        rest_error($request);
        break;
}
?>